package com.ts.exception.oauth;

import com.ts.exception.TSException;

/**
 * Base parent class for all OAuth exceptions.
 */

public class OAuthException extends TSException {
  private static final long serialVersionUID = 2L;

  public OAuthException (String code, String description, String requestId, Integer statusCode,
      Throwable e) {
    super(description, requestId, code, statusCode, e);
  }
}