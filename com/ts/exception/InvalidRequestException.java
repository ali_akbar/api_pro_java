package com.ts.exception;

public class InvalidRequestException extends TSException {
  private static final long serialVersionUID = 2L;

  private final String param;

  // Constructs a new InvalidRequestException with the specified details.
  
  public InvalidRequestException (String message, String param, String requestId, String code,
      Integer statusCode, Throwable e) {
    super(message, requestId, code, statusCode, e);
    this.param = param;
  }

  public String getParam() {
    return param;
  }
}